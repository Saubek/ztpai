package com.example.ztpai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class AAAZtpaiApplication {

    public static void main(String[] args) throws Throwable {
        SpringApplication.run(AAAZtpaiApplication.class, args);
    }

}
