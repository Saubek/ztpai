package com.example.ztpai.controllers;

import com.example.ztpai.exceptions.UserNotFoundException;
import com.example.ztpai.models.User;
import com.example.ztpai.repository.UserDetailRepository;
import com.example.ztpai.repository.UserRepository;
import org.springframework.hateoas.EntityModel;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Controller
public class UserController {

	private final UserDetailRepository userDetailRepository;
	private final UserRepository userRepository;

	UserController(UserDetailRepository userDetailRepository, UserRepository userRepository) {
		this.userDetailRepository = userDetailRepository;
		this.userRepository = userRepository;
	}

	@GetMapping("/users")
	List<User> all() {
		return userRepository.findAll();
	}


	@DeleteMapping("/users/{id}")
	void deleteUser(@PathVariable Long id) {
		userRepository.deleteById(id);
	}


	@GetMapping("/users/{id}")
	EntityModel<User> one(@PathVariable Long id) {
		User user = userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
		return EntityModel.of(user,
				linkTo(methodOn(UserController.class).one(id)).withSelfRel(),
				linkTo(methodOn(UserController.class).all()).withRel("users")
		);
	}

}

