package com.example.ztpai.registration;

import com.example.ztpai.models.User;
import com.example.ztpai.models.UserDetail;
import com.example.ztpai.repository.UserRepository;
import com.example.ztpai.services.UserService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class RegistrationService {

	private final UserService userService;
	private final EmailValidator emailValidator;
	private final UserRepository userRepository;

	public RegistrationService(UserService userService, EmailValidator emailValidator, UserRepository userRepository) {
		this.userService = userService;
		this.emailValidator = emailValidator;
		this.userRepository = userRepository;
	}


	public void register(RegistrationRequest request) {
		boolean isValidEmail = emailValidator.test(request.getUser_mail());
		if(!isValidEmail){
			throw new IllegalStateException("Email not valid.");
		}
		if(userRepository.findByEmail(request.getUser_mail()).isPresent())
		{
			throw new UsernameNotFoundException("0");
		}

		userService.signUpUser(
				new User(
					request.getUser_mail(),
					request.getUser_password()
				),
				new UserDetail(
					request.getUser_name(),
					request.getUser_surname(),
					request.getUser_phone()
				)
		);
	}
}
