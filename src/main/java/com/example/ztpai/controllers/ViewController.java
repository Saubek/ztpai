package com.example.ztpai.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class ViewController {

	@GetMapping(path = "login")
	public String getLoginView() {
		return "login";
	}

	@GetMapping(path = "index")
	public String getIndexView() {
		return "index";
	}

	@GetMapping(path = "contact")
	public String getContactView() {
		return "contact";
	}

	@GetMapping(path = "register")
	public String getRegisterView() {
		return "register";
	}

	@GetMapping(path = "about")
	public String getAboutView() {
		return "about";
	}

	@GetMapping(path = "magazine")
	public String getMagazineView() {
		return "magazine";
	}

}
