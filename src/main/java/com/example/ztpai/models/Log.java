package com.example.ztpai.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "logs")
public class Log {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long log_id;
    private String email;
    private Date datetime;
    private String device;

    public Log() {
    }

    public Log(String email, Date datetime, String device) {
        this.email = email;
        this.datetime = datetime;
        this.device = device;
    }

    public Long getLog_id() {
        return log_id;
    }

    public String getEmail() {
        return email;
    }

    public Date getDatetime() {
        return datetime;
    }

    public String getDevice() {
        return device;
    }

    public void setLog_id(Long log_id) {
        this.log_id = log_id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public void setDevice(String device) {
        this.device = device;
    }
}
