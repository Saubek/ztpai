package com.example.ztpai.enums;

public enum UserRole {
    ROLE_ADMIN,
    ROLE_USER
}
