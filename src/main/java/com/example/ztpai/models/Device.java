package com.example.ztpai.models;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "devices")
public class Device {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long device_id;
    private String device_name;
    private Date datetime;
    private Long assigned_by_id;

    @ManyToMany(mappedBy = "borrowedDevices")
    private Set<User> deviceBorrowedBy;

    public Device() {
    }

    public Device(String device_name, Date datetime, Long assigned_by_id) {
        this.device_name = device_name;
        this.datetime = datetime;
        this.assigned_by_id = assigned_by_id;
    }

    public Long getDevice_id() {
        return device_id;
    }

    public String getDevice_name() {
        return device_name;
    }

    public Date getDatetime() {
        return datetime;
    }

    public Long getAssigned_by_id() {
        return assigned_by_id;
    }

    public void setDevice_id(Long device_id) {
        this.device_id = device_id;
    }

    public void setDevice_name(String device_name) {
        this.device_name = device_name;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public void setAssigned_by_id(Long assigned_by_id) {
        this.assigned_by_id = assigned_by_id;
    }
}
