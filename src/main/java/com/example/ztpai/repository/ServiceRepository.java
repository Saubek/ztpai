package com.example.ztpai.repository;

import com.example.ztpai.enums.ServiceType;
import com.example.ztpai.models.Service;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ServiceRepository extends JpaRepository<Service, Long> {

	@Query("select s from Service s where s.type = ?1")
	List<Service> findByType(ServiceType type);

	@Query("select s from Service s where s.assigned_by_id = ?1")
	List<Service> findByUserId(Long id);
}
