package com.example.ztpai.models;


import com.example.ztpai.enums.UserRole;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

@Entity
@Table(name = "users")
public class User implements UserDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long user_id;

	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "user")
	@JsonManagedReference
	private UserDetail user_id_detail;

	@Enumerated(EnumType.STRING)
	private UserRole role;

	@Column(nullable = false, unique = true)
	private String user_mail;
	@Column(nullable = false)
	private String user_password;
	private Boolean locked = false;
	private Boolean enabled = true;

	@ManyToMany
	@JoinTable(
			name = "users_devices",
			joinColumns = @JoinColumn(name = "user_id"),
			inverseJoinColumns = @JoinColumn(name = "device_id")
	)
	private Set<Device> borrowedDevices;

	@ManyToMany
	@JoinTable(
			name = "users_services",
			joinColumns = @JoinColumn(name = "user_id"),
			inverseJoinColumns = @JoinColumn(name = "service_id")
	)
	private Set<Service> borrowedServices;


	public User() {
	}

	public User(String user_mail, String user_password) {
		super();
		this.role = UserRole.ROLE_USER;
		this.user_mail = user_mail;
		this.user_password = user_password;
	}

	public Long getUser_id() {
		return user_id;
	}

	public void setUser_id(Long user_id) {
		this.user_id = user_id;
	}

	public UserDetail getUser_id_detail() {
		return user_id_detail;
	}

	public void setUser_id_detail(UserDetail user_id_detail) {
		this.user_id_detail = user_id_detail;
	}

	public void setPassword(String user_password) {
		this.user_password = user_password;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		SimpleGrantedAuthority authority =
				new SimpleGrantedAuthority(role.name());
		return Collections.singletonList(authority);
	}

	@Override
	public String getPassword() {
		return user_password;
	}

	@Override
	public String getUsername() {
		return user_mail;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return !locked;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}
}
