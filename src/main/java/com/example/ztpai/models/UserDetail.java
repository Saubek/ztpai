package com.example.ztpai.models;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "user_details")
public class UserDetail implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long user_detail_id;
    private String user_name;
    private String user_surname;
    private String user_phone;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false, unique = true)
    @JsonBackReference
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getUser_detail_id() {
        return user_detail_id;
    }

    public void setUser_detail_id(Long user_detail_id) {
        this.user_detail_id = user_detail_id;
    }

    public UserDetail() {
    }

    public UserDetail(String user_name, String user_surname, String user_phone) {
        super();
        this.user_name = user_name;
        this.user_surname = user_surname;
        this.user_phone = user_phone;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getUser_surname() {
        return user_surname;
    }

    public String getUser_phone() {
        return user_phone;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public void setUser_surname(String user_surname) {
        this.user_surname = user_surname;
    }

    public void setUser_phone(String user_phone) {
        this.user_phone = user_phone;
    }
}
