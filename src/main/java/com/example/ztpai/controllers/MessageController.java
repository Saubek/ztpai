package com.example.ztpai.controllers;

import com.example.ztpai.models.Message;
import com.example.ztpai.repository.MessageRepository;
import com.example.ztpai.repository.UserRepository;
import com.example.ztpai.security.SecurityController;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/")
public class MessageController {

    private final MessageRepository messageRepository;
    private final  UserRepository userRepository;

    SecurityController securityController = new SecurityController();


    MessageController(MessageRepository repository, UserRepository userRepository) {
        this.messageRepository = repository;
        this.userRepository = userRepository;
    }

    @GetMapping(path = "messages")
    public String getMessages(Model model) {
        model.addAttribute("messages", messageRepository.findAll());
        Message message = new Message();
        model.addAttribute("messadd", message);
        return "messages";
    }

    @PostMapping(path = "messages")
    public String addMessage(@ModelAttribute("messadd") Message message) {
        String author = securityController.currentUserName(SecurityContextHolder.getContext().getAuthentication());
        message.setMessage_author(userRepository.findByEmail(author).get().getUsername());
        System.out.println(author);
        messageRepository.save(message);
        return "redirect:/messages";
    }
}
