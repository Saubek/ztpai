package com.example.ztpai.repository;

import com.example.ztpai.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	@Query("select u from User u where u.user_mail = ?1")
	Optional<User> findByEmail(String user_mail);

}
