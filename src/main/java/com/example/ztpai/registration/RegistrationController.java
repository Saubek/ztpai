package com.example.ztpai.registration;

import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController

public class RegistrationController {

	public final RegistrationService registrationService;
	public RegistrationRequest request;

	public RegistrationController(RegistrationService registrationService) {
		this.registrationService = registrationService;
	}

	@RequestMapping(value = "register")
	public void register(@RequestParam Map<String, String> body){
		request=new RegistrationRequest(
				body.get("username"),
				body.get("password"),
				body.get("name"),
				body.get("surname"),
				body.get("phone")
				);

		registrationService.register(this.request);
	}

}
