package com.example.ztpai.models;

import javax.persistence.*;

@Entity
@Table(name = "messages")
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long message_id;
    @Column(nullable = false)
    private String message_author;
    @Column(nullable = false)
    private String message_text;


    public Message() {
    }

    public Message(String message_author, String message_text) {
        this.message_author = message_author;
        this.message_text = message_text;
    }

    public Long getMessage_id() {
        return message_id;
    }

    public String getMessage_author() {
        return message_author;
    }

    public String getMessage_text() {
        return message_text;
    }

    public void setMessage_id(Long message_id) {
        this.message_id = message_id;
    }

    public void setMessage_author(String message_author) {
        this.message_author = message_author;
    }

    public void setMessage_text(String message_text) {
        this.message_text = message_text;
    }
}
