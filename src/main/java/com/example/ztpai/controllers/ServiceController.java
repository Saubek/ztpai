package com.example.ztpai.controllers;

import com.example.ztpai.enums.ServiceType;
import com.example.ztpai.models.Service;
import com.example.ztpai.repository.ServiceRepository;
import com.example.ztpai.repository.UserRepository;
import com.example.ztpai.security.SecurityController;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/")
public class ServiceController {

	public ServiceController(ServiceRepository serviceRepository, UserRepository userRepository) {
		this.serviceRepository = serviceRepository;
		this.userRepository = userRepository;
	}

	SecurityController securityController = new SecurityController();

	private final ServiceRepository serviceRepository;
	com.example.ztpai.repository.UserRepository userRepository;

	@GetMapping(path = "gym")
	public String getGym(Model model) {
		model.addAttribute("gym", serviceRepository.findByType(ServiceType.GYM));
		Service service = new Service();
		model.addAttribute("gymadd", service);
		return "gym";
	}

	@PostMapping(path = "gym")
	public String addGym(@ModelAttribute("gymadd") Service service) {
		String username = securityController.currentUserName(SecurityContextHolder.getContext().getAuthentication());
		service.setAssigned_by_id(userRepository.findByEmail(username).get().getUser_id());
		service.setType(ServiceType.GYM);
		serviceRepository.save(service);
		return "redirect:/gym";
	}

	@GetMapping(path = "bilard")
	public String getBilard(Model model) {
		model.addAttribute("bilard", serviceRepository.findByType(ServiceType.BILARD));
		Service service = new Service();
		model.addAttribute("bilardadd", service);
		return "bilard";
	}

	@PostMapping(path = "bilard")
	public String addBilard(@ModelAttribute("bilardadd") Service service) {
		String username = securityController.currentUserName(SecurityContextHolder.getContext().getAuthentication());
		service.setAssigned_by_id(userRepository.findByEmail(username).get().getUser_id());
		service.setType(ServiceType.BILARD);
		serviceRepository.save(service);
		return "redirect:/bilard";
	}

	@GetMapping(path = "pingpong")
	public String getPingpong(Model model) {
		model.addAttribute("pingpong", serviceRepository.findByType(ServiceType.PINGPONG));
		Service service = new Service();
		model.addAttribute("pingpongadd", service);
		return "pingpong";
	}

	@PostMapping(path = "pingpong")
	public String addPingpong(@ModelAttribute("pingpongadd") Service service) {
		String username = securityController.currentUserName(SecurityContextHolder.getContext().getAuthentication());
		service.setAssigned_by_id(userRepository.findByEmail(username).get().getUser_id());
		service.setType(ServiceType.PINGPONG);
		serviceRepository.save(service);
		return "redirect:/pingpong";
	}

	@GetMapping(path = "basement")
	public String getBasement(Model model) {
		model.addAttribute("basement", serviceRepository.findByType(ServiceType.BASEMENT));
		Service service = new Service();
		model.addAttribute("basementadd", service);
		return "basement";
	}

	@PostMapping(path = "basement")
	public String addBasement(@ModelAttribute("basementadd") Service service) {
		String username = securityController.currentUserName(SecurityContextHolder.getContext().getAuthentication());
		service.setAssigned_by_id(userRepository.findByEmail(username).get().getUser_id());
		service.setType(ServiceType.BASEMENT);
		serviceRepository.save(service);
		return "redirect:/basement";
	}

	@GetMapping(path = "washroom")
	public String getWashroom(Model model) {
		model.addAttribute("washroom", serviceRepository.findByType(ServiceType.WASHROOM));
		Service service = new Service();
		model.addAttribute("washroomadd", service);
		return "washroom";
	}

	@PostMapping(path = "washroom")
	public String addWashroom(@ModelAttribute("washroomadd") Service service) {
		String username = securityController.currentUserName(SecurityContextHolder.getContext().getAuthentication());
		service.setAssigned_by_id(userRepository.findByEmail(username).get().getUser_id());
		service.setType(ServiceType.WASHROOM);
		serviceRepository.save(service);
		return "redirect:/washroom";
	}

	@GetMapping(path = "service")
	public String getUserServices(Model model) {
		Service service = new Service();
		//Long variableDelete = new Long(0);
		String username = securityController.currentUserName(SecurityContextHolder.getContext().getAuthentication());
		model.addAttribute("service", serviceRepository.findByUserId(userRepository.findByEmail(username).get().getUser_id()));
		//model.addAttribute("serviceDelete", variableDelete);
		return "service";
	}

	@GetMapping(path = "service/{id}")
	public String deleteService(@PathVariable("id") Long id) {
		serviceRepository.deleteById(id);
		return "redirect:/service";
	}
}
