package com.example.ztpai.registration;

public class RegistrationRequest {

	private String user_mail;
	private String user_password;
	private String user_name;
	private String user_surname;
	private String user_phone;


	public RegistrationRequest(String user_mail, String user_password, String user_name, String user_surname, String user_phone) {
		this.user_mail = user_mail;
		this.user_password = user_password;
		this.user_name = user_name;
		this.user_surname = user_surname;
		this.user_phone = user_phone;
	}

	public String getUser_mail() {
		return user_mail;
	}

	public String getUser_password() {
		return user_password;
	}

	public String getUser_name() {
		return user_name;
	}

	public String getUser_surname() {
		return user_surname;
	}

	public String getUser_phone() {
		return user_phone;
	}

	public void setUser_mail(String user_mail) {
		this.user_mail = user_mail;
	}

	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public void setUser_surname(String user_surname) {
		this.user_surname = user_surname;
	}

	public void setUser_phone(String user_phone) {
		this.user_phone = user_phone;
	}



}
