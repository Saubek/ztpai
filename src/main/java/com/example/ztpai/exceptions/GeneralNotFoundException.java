package com.example.ztpai.exceptions;

public class GeneralNotFoundException extends RuntimeException {
    public GeneralNotFoundException(Long id) {
        super("Not found.");
    }
}
