package com.example.ztpai.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class GeneralNotFoundAdvice {
    @ResponseBody
    @ExceptionHandler(GeneralNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String generalNotFoundHandler(GeneralNotFoundException ex) {
        return ex.getMessage();
    }


}
