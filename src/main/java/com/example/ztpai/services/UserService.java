package com.example.ztpai.services;

import com.example.ztpai.controllers.ViewController;
import com.example.ztpai.models.User;
import com.example.ztpai.models.UserDetail;
import com.example.ztpai.repository.UserDetailRepository;
import com.example.ztpai.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

@Service
public class UserService implements UserDetailsService {

	@Autowired
	private final UserRepository userRepository;
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	private final UserDetailRepository userDetailRepository;

	public UserService(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder, UserDetailRepository userDetailRepository) {
		this.userRepository = userRepository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.userDetailRepository = userDetailRepository;
	}

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		return userRepository.findByEmail(email)
				.orElseThrow(() -> new UsernameNotFoundException("User with email " + email + "not found."));
	}

	@GetMapping("messages")
	public void signUpUser(User user, UserDetail userDetail) {
		boolean userExists = userRepository
				.findByEmail(user.getUsername())
				.isPresent();

		if (userExists) {
			throw new IllegalStateException("Email already taken.");
		}
		String encodedPassword = bCryptPasswordEncoder.encode(user.getPassword());
		user.setPassword(encodedPassword);

		userRepository.save(user);
		user.setUser_id_detail(userDetail);
		userDetail.setUser(user);
		userDetailRepository.save(userDetail);

	}
}
