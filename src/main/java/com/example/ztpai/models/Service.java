package com.example.ztpai.models;

import com.example.ztpai.enums.ServiceType;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity
@Table(name = "services")
public class Service {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long service_id;
    private String service_name;
    private String service_room;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private String datetime;
    @DateTimeFormat(pattern = "HH:mm:ss")
    private String hour;
    private Long assigned_by_id;
    @Enumerated(EnumType.STRING)
    private ServiceType type;

    @ManyToMany(mappedBy = "borrowedServices")
    private Set<User> serviceBorrowedBy;

    public Service() {
    }

    public Service(String service_name, String service_room, String datetime, String hour, Long assigned_by_id, ServiceType type) {
        this.service_name = service_name;
        this.service_room = service_room;
        this.datetime = datetime;
        this.hour = hour;
        this.assigned_by_id = assigned_by_id;
        this.type = type;
    }

    public Long getService_id() {
        return service_id;
    }

    public String getService_name() {
        return service_name;
    }

    public String getService_room() {
        return service_room;
    }

    public String getDatetime() {
        return datetime;
    }

    public String getHour() { return hour; }

    public Long getAssigned_by_id() {
        return assigned_by_id;
    }

    public ServiceType getType() {
        return type;
    }

    public void setService_id(Long service_id) {
        this.service_id = service_id;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public void setService_room(String service_room) {
        this.service_room = service_room;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public void setHour(String hour) { this.hour = hour; }

    public void setAssigned_by_id(Long assigned_by_id) {
        this.assigned_by_id = assigned_by_id;
    }

    public void setType(ServiceType type) {
        this.type = type;
    }
}
